def create(*args,**kwargs):
    if db="postgresql":
        print "Create query of postgres here"
    elif db="sqlite":
        print "Create query of sqlite here"
    elif db="mysql":
        print "Create query of mysql here"

def read(*args,**kwargs):
    if db="postgresql":
        print "Read query of postgres here"
    elif db="sqlite":
        print "Read query of sqlite here"
    elif db="mysql":
        print "Read query of mysql here"

def update(*args,**kwargs):
    if db="postgresql":
        print "Update query of postgres here"
    elif db="sqlite":
        print "Update query of sqlite here"
    elif db="mysql":
        print "Update query of mysql here"

def delete(*args,**kwargs):
    if db="postgresql":
        print "Delete query of postgres here"
    elif db="sqlite":
        print "Delete query of sqlite here"
    elif db="mysql":
        print "Delete query of mysql here"